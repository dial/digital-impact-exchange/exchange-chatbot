import os

import requests
from fake_useragent import UserAgent
from langchain.indexes import SQLRecordManager, index
from langchain_community.document_loaders.web_base import WebBaseLoader
from langchain_text_splitters import RecursiveCharacterTextSplitter

from app.vector import prepare_vector_store


def parse_and_index_exchange_api(exchange_entity_name, vector_store, record_manager):
    ua = UserAgent()
    headers = {"User-Agent": ua.chrome}

    exchange_api_base = os.environ["EXCHANGE_API_BASE"]
    exchange_api_query = "?page={current_page}"

    current_page = 1
    while True:
        current_query = exchange_api_query.format(current_page=current_page)

        url_to_process = f"{exchange_api_base}/{exchange_entity_name}{current_query}"
        print(f"Processing URL: {url_to_process}.")

        response = requests.get(url_to_process, headers=headers)
        exchange_api_json = response.json()

        exchange_records = exchange_api_json["results"]

        if len(exchange_records) == 0:
            print(" Last page reached. Exiting.")
            break

        print(f"  Received {len(exchange_records)} records.")

        # Pull link from the exchange records
        exchange_record_urls = map(
            lambda exchange_record: exchange_record["self_url"], exchange_records
        )
        for exchange_record_url in exchange_record_urls:
            print(f"  Parsing API: {exchange_record_url}.")
            parse_and_index_url(
                url=exchange_record_url, vector_store=vector_store, record_manager=record_manager
            )

        current_page = current_page + 1


def parse_and_index_url(url, vector_store, record_manager):
    loader = WebBaseLoader(url)
    base_documents = loader.load()

    text_splitter = RecursiveCharacterTextSplitter()
    documents = text_splitter.split_documents(base_documents)

    print(f"  Processing {len(documents)} documents. This may take a while. Please wait...")
    indexing_result = index(
        docs_source=documents[:2],
        record_manager=record_manager,
        vector_store=vector_store,
        cleanup="incremental",
        source_id_key="source",
    )
    print(f"  Indexing result: {indexing_result}.")

    vector_store.save_local(
        folder_path=os.environ["FAISS_INDEX_PATH"], index_name="exchange-chatbot-index"
    )


vector_store = prepare_vector_store()

namespace = "faiss/exchange-chatbot-index"
record_manager = SQLRecordManager(
    namespace, db_url=f"sqlite:///{os.environ['RECORD_MANAGER_PATH']}/record_manager_cache.sql"
)
record_manager.create_schema()

parse_and_index_exchange_api(
    exchange_entity_name="products",
    vector_store=vector_store,
    record_manager=record_manager,
)

parse_and_index_exchange_api(
    exchange_entity_name="building_blocks",
    vector_store=vector_store,
    record_manager=record_manager,
)

parse_and_index_exchange_api(
    exchange_entity_name="use_cases",
    vector_store=vector_store,
    record_manager=record_manager,
)


ua = UserAgent()
headers = {"User-Agent": ua.chrome}

wp_base = "https://dial.global"
wp_query = "?page={current_page}"

# Processing research article pages.
current_page = 1
wp_research_base = "/wp-json/wp/v2/research"
while True:
    current_query = wp_query.format(current_page=current_page)

    url_to_process = f"{wp_base}{wp_research_base}{current_query}"
    print(f"Processing URL: {url_to_process}.")

    response = requests.get(url_to_process, headers=headers)
    research_records = response.json()

    if isinstance(research_records, dict):
        print(" Last page reached. Exiting.")
        break

    print(f"  Received {len(research_records)} records.")

    # Pull every link from the research record
    research_record_urls = map(lambda research_record: research_record["link"], research_records)
    for research_record_url in research_record_urls:
        print(f"  Parsing research: {research_record_url}.")
        parse_and_index_url(
            url=research_record_url, vector_store=vector_store, record_manager=record_manager
        )

    current_page = current_page + 1

# Processing news article pages.
current_page = 1
wp_post_base = "/wp-json/wp/v2/posts"
while True:
    current_query = wp_query.format(current_page=current_page)

    url_to_process = f"{wp_base}{wp_post_base}{current_query}"
    print(f"Processing URL: {url_to_process}.")

    response = requests.get(url_to_process, headers=headers)
    post_records = response.json()

    if isinstance(post_records, dict):
        print(" Last page reached. Exiting.")
        break

    print(f"  Received {len(post_records)} records.")

    # Pull every link from the research record
    post_record_urls = map(lambda post_record: post_record["link"], post_records)
    for post_record_url in post_record_urls:
        print(f"  Parsing post: {post_record_url}.")
        parse_and_index_url(
            url=post_record_url, vector_store=vector_store, record_manager=record_manager
        )

    current_page = current_page + 1

# Processing news article pages.
current_page = 1
wp_post_base = "/wp-json/wp/v2/pages"
while True:
    current_query = wp_query.format(current_page=current_page)

    url_to_process = f"{wp_base}{wp_post_base}{current_query}"
    print(f"Processing URL: {url_to_process}.")

    response = requests.get(url_to_process, headers=headers)
    post_records = response.json()

    if isinstance(post_records, dict):
        print(" Last page reached. Exiting.")
        break

    print(f"  Received {len(post_records)} records.")

    # Pull every link from the research record
    post_record_urls = map(lambda post_record: post_record["link"], post_records)
    for post_record_url in post_record_urls:
        print(f"  Parsing post: {post_record_url}.")
        parse_and_index_url(
            url=post_record_url, vector_store=vector_store, record_manager=record_manager
        )

    current_page = current_page + 1

wp_base = "https://digitalprinciples.org"

current_page = 1
wp_post_base = "/wp-json/wp/v2/posts"
while True:
    current_query = wp_query.format(current_page=current_page)

    url_to_process = f"{wp_base}{wp_post_base}{current_query}"
    print(f"Processing URL: {url_to_process}.")

    response = requests.get(url_to_process, headers=headers)
    post_records = response.json()

    if isinstance(post_records, dict):
        print(" Last page reached. Exiting.")
        break

    print(f"  Received {len(post_records)} records.")

    # Pull every link from the research record
    post_record_urls = map(lambda post_record: post_record["link"], post_records)
    for post_record_url in post_record_urls:
        print(f"  Parsing post: {post_record_url}.")
        parse_and_index_url(
            url=post_record_url, vector_store=vector_store, record_manager=record_manager
        )

    current_page = current_page + 1

current_page = 1
wp_post_base = "/wp-json/wp/v2/pages"
while True:
    current_query = wp_query.format(current_page=current_page)

    url_to_process = f"{wp_base}{wp_post_base}{current_query}"
    print(f"Processing URL: {url_to_process}.")

    response = requests.get(url_to_process, headers=headers)
    post_records = response.json()

    if isinstance(post_records, dict):
        print(" Last page reached. Exiting.")
        break

    print(f"  Received {len(post_records)} records.")

    # Pull every link from the research record
    post_record_urls = map(lambda post_record: post_record["link"], post_records)
    for post_record_url in post_record_urls:
        print(f"  Parsing post: {post_record_url}.")
        parse_and_index_url(
            url=post_record_url, vector_store=vector_store, record_manager=record_manager
        )

    current_page = current_page + 1
