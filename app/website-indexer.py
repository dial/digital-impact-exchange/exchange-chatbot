import os

import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from langchain.indexes import SQLRecordManager, index
from langchain_community.document_loaders.recursive_url_loader import RecursiveUrlLoader
from langchain_text_splitters import RecursiveCharacterTextSplitter
from requests.exceptions import ConnectionError

from app.vector import prepare_vector_store


def parse_and_index_url_recursively(url, vector_store, record_manager):
    loader = RecursiveUrlLoader(
        url=url,
        max_depth=1,
        prevent_outside=True,
        extractor=lambda x: BeautifulSoup(x, "html.parser").text,
    )
    base_documents = loader.load()

    text_splitter = RecursiveCharacterTextSplitter()
    documents = text_splitter.split_documents(base_documents)

    print(f"  Processing {len(documents)} documents. This may take a while. Please wait...")
    indexing_result = index(
        docs_source=documents[:2],
        record_manager=record_manager,
        vector_store=vector_store,
        cleanup="incremental",
        source_id_key="source",
    )
    print(f"  Indexing result: {indexing_result}.")

    vector_store.save_local(
        folder_path=os.environ["FAISS_INDEX_PATH"], index_name="exchange-chatbot-index"
    )


vector_store = prepare_vector_store()

namespace = "faiss/exchange-chatbot-index"
record_manager = SQLRecordManager(
    namespace, db_url=f"sqlite:///{os.environ['RECORD_MANAGER_PATH']}/record_manager_cache.sql"
)
record_manager.create_schema()

ua = UserAgent()
headers = {"User-Agent": ua.chrome}

exchange_api_base = os.environ["EXCHANGE_API_BASE"]
exchange_api_query = "?page={current_page}"

current_page = 1
exchange_entity_name = "products"
while True:
    current_query = exchange_api_query.format(current_page=current_page)

    url_to_process = f"{exchange_api_base}/{exchange_entity_name}{current_query}"
    print(f"Processing URL: {url_to_process}.")

    response = requests.get(url_to_process, headers=headers)
    exchange_api_json = response.json()

    exchange_records = exchange_api_json["results"]

    if len(exchange_records) == 0:
        print(" Last page reached. Exiting.")
        break

    print(f"  Received {len(exchange_records)} records.")

    # Pull website from the exchange records
    exchange_record_websites = map(
        lambda exchange_record: exchange_record["website"], exchange_records
    )
    for exchange_record_website in exchange_record_websites:
        if exchange_record_website is None or exchange_record_website == "":
            continue
        print(f"  Parsing website: {exchange_record_website}.")
        valid_exchange_record_website = exchange_record_website.replace("http://", "")
        if valid_exchange_record_website == "":
            continue

        try:
            # Try opening the website and skip if we can't reach it.
            requests.get(f"https://{valid_exchange_record_website}", headers=headers)
            parse_and_index_url_recursively(
                url=f"https://{valid_exchange_record_website}",
                vector_store=vector_store,
                record_manager=record_manager,
            )
        except ConnectionError:
            print(f"  Website {valid_exchange_record_website} not reachable.")

    current_page = current_page + 1
