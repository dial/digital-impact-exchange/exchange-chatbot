import os

from faiss import IndexFlatL2
from langchain_community.docstore.in_memory import InMemoryDocstore
from langchain_community.vectorstores.faiss import FAISS
from langchain_openai import OpenAIEmbeddings


def prepare_vector_store():
    # Size:
    # 1536 for text-embedding-3-small
    # 3072 for text-embedding-3-large
    print("Preparing vector store!")
    embeddings = OpenAIEmbeddings()
    faiss = FAISS(
        embedding_function=embeddings,
        index=IndexFlatL2(1536),
        docstore=InMemoryDocstore(),
        index_to_docstore_id={},
    )
    try:
        vector_store = faiss.load_local(
            allow_dangerous_deserialization=True,
            folder_path=os.environ["FAISS_INDEX_PATH"],
            index_name="exchange-chatbot-index",
            embeddings=embeddings,
        )
    except RuntimeError:
        print("Index not found. Creating new index.")
        faiss.save_local(
            folder_path=os.environ["FAISS_INDEX_PATH"], index_name="exchange-chatbot-index"
        )
        vector_store = faiss.load_local(
            allow_dangerous_deserialization=True,
            folder_path=os.environ["FAISS_INDEX_PATH"],
            index_name="exchange-chatbot-index",
            embeddings=embeddings,
        )
    return vector_store
