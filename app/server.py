from typing import List

from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from langchain.chains import create_history_aware_retriever, create_retrieval_chain
from langchain.chains.combine_documents import create_stuff_documents_chain
from langchain.tools.retriever import create_retriever_tool
from langchain_community.chat_message_histories import ChatMessageHistory
from langchain_core.chat_history import BaseChatMessageHistory
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langchain_core.runnables.history import RunnableWithMessageHistory
from langchain_openai import ChatOpenAI
from langserve import add_routes
from pydantic.v1 import BaseModel

from app.vector import prepare_vector_store

vector_store = prepare_vector_store()
retriever = vector_store.as_retriever()

retriever_tool = create_retriever_tool(
    retriever,
    "exchange-chatbot",
    """
    An online resource hub offering data, insights, and tools to help policymakers, 
    governments and development actors to advance progress on digital public infrastructure.
    """,
)

tools = [retriever_tool]
llm = ChatOpenAI()

contextualize_q_system_prompt = """Given a chat history and the latest user question
which might reference context in the chat history, formulate a standalone question
which can be understood without the chat history. Do NOT answer the question,
just reformulate it if needed and otherwise return it as is."""
contextualize_q_prompt = ChatPromptTemplate.from_messages(
    [
        ("system", contextualize_q_system_prompt),
        MessagesPlaceholder("history"),
        ("human", "{input}"),
    ]
)

history_aware_retriever = create_history_aware_retriever(
    llm, retriever, contextualize_q_prompt
)

qa_system_prompt = """You are an assistant for question-answering tasks.
Use the following pieces of retrieved context to answer the question.
If you don't know the answer, just say that you don't know.

{context}"""
qa_prompt = ChatPromptTemplate.from_messages(
    [
        ("system", qa_system_prompt),
        MessagesPlaceholder("history"),
        ("human", "{input}"),
    ]
)


qa_chain = create_stuff_documents_chain(llm, qa_prompt)
rag_chain = create_retrieval_chain(history_aware_retriever, qa_chain)

chat_message_history_store = {}

def get_session_history(session_id: str) -> BaseChatMessageHistory:
    if session_id not in chat_message_history_store:
        chat_message_history_store[session_id] = ChatMessageHistory()
    return chat_message_history_store[session_id]


conversational_rag_chain = RunnableWithMessageHistory(
    rag_chain,
    get_session_history,
    input_messages_key="input",
    history_messages_key="history",
    output_messages_key="answer",
)

app = FastAPI(
    title="Exchange Chatbot Server",
    version="1.0",
    description="A simple API server using LangChain.",
)

# User input
class ChatInput(BaseModel):
    """Chat input with the bot."""
    input: str
    
class ChatOutput(BaseModel):
    """Chat output from the bot."""
    input: str
    answer: str
    history: List
    context: List

@app.get("/")
async def redirect_root_to_docs():
    return RedirectResponse("/docs")

add_routes(
    app,
    # conversational_rag_chain.with_types(input_type=ChatInput, output_type=ChatOutput),
    conversational_rag_chain,
    path="/retriever",
)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8080)
