# The Exchange Simple Chatbot

## Installation

Install the LangChain CLI if you haven't yet

```bash
pip install -U langchain-cli
```

Install Poetry

Readme for the installation steps: https://python-poetry.org/docs/#installation

## Launch Indexer

```bash
export OPENAI_API_KEY="sk-..."
poetry run python app/indexer.py
```
This will create local index directory ```faiss-index```. The index will be used
by the retriever when we run the retriever.

**TODO:**
* Add more url to index
* Configurable list of url to index
  * Using config file or database?
* Crawler that smart enough to recursively traverse base URL
  * https://python.langchain.com/docs/integrations/document_loaders/recursive_url/
* Cron / scheduler to run the indexer periodically.

## Launch LangServe

```bash
export OPENAI_API_KEY="sk-..."
poetry install
poetry run langchain serve 
```

Langserve playground will be served on: http://localhost:8000/retriever/playground/

## Adding dependency packages

```bash
poetry add [package-name]
```

## Running in Docker

This project folder includes a Dockerfile that allows you to easily build and host your LangServe app.

### Building the Image

To build the image, you simply:

```shell
docker build . -t my-langserve-app
```

If you tag your image with something other than `my-langserve-app`,
note it for use in the next step.

### Running the Image Locally

To run the image, you'll need to include any environment variables
necessary for your application.

In the below example, we inject the `OPENAI_API_KEY` environment
variable with the value set in my local environment
(`$OPENAI_API_KEY`)

We also expose port 8080 with the `-p 8080:8080` option.

```shell
docker run -e OPENAI_API_KEY=$OPENAI_API_KEY -p 8080:8080 my-langserve-app
```
