FROM python:3.11-slim

RUN pip install poetry==1.6.1
RUN poetry config virtualenvs.create false

WORKDIR /code

COPY ./pyproject.toml ./README.md ./poetry.lock* ./
COPY ./package[s] ./packages

RUN poetry install  --no-interaction --no-ansi --no-root

COPY ./app ./app

RUN poetry install --no-interaction --no-ansi

RUN apt-get update
RUN apt-get install -y cron sqlite3

COPY cron-indexer /etc/cron.d/cron-indexer
RUN crontab /etc/cron.d/cron-indexer

COPY ./print-env.sh ./print-env.sh

EXPOSE 8080
CMD ./print-env.sh && exec uvicorn app.server:app --host 0.0.0.0 --port 8080
