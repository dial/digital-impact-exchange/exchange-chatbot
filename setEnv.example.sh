#!/bin/bash

if [ $# -eq 0 ]
then
    read -p "Which environment (dev, prod): " user_env
    user_env="${user_env:-prod}"
else
    user_env=$1
fi

if [ "${user_env:0:1}" == "d" ] || [ "${user_env:0:1}" == "dev" ]; then
    export OPENAI_API_KEY=<your openai api key>
    export RECORD_MANAGER_PATH='record-manager'
    export FAISS_INDEX_PATH='faiss-index'
    export EXCHANGE_API_BASE=<your exchange api base url>
fi
if [ "${user_env:0:1}" == "p" ] || [ "${user_env:0:1}" == "prod" ]; then
    export OPENAI_API_KEY=<your openai api key>
    export RECORD_MANAGER_PATH='record-manager'
    export FAISS_INDEX_PATH='faiss-index'
    export EXCHANGE_API_BASE=<your exchange api base url>
fi
